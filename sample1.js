/**********************\
|  Sample 1: Javascrpt |
\**********************/

;;; App = ( __ => {
    // document constant: _
    const _ = __.document

    // Application initialization function
    this.init = () => {
        let blurb = _.getElementById("blurb");
        blurb.innerText += " JAVASCRIPT";
    };

    // More sh!t goes here


    // Return the `App` object
    return this;
})(window);
